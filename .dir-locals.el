; Copyright 2021 Citrix
; SPDX-License-Identifier: MIT OR Apache-2.0
; There is NO WARRANTY.

((rust-mode . ((rust-indent-offset . 2)))
 (css-mode . ((css-indent-offset . 2)))
 (js-mode . ((js-indent-level . 2))))
