// Copyright 2021 Citrix
// SPDX-License-Identifier: MIT OR Apache-2.0
// There is NO WARRANTY.

//! Uniform access to github and gitlab
//!
//! Currently, listing and creating merge requests is suppored.
//!
//! Example
//!
//! ```
//! use gitforge::forge;
//!
//! let mut f = forge::Config {
//!   kind: "github".parse().ok(),
//!   host: "github.com".into(),
//!   ..Default::default()
//! }
//!   .load_default_token().unwrap()
//!   .forge().unwrap();
//!
//! let req = forge::Req::MergeRequests(forge::Req_MergeRequests{
//!   target_repo: "CVEProject/cvelist".into(),
//!   statuses: Some([forge::IssueMrStatus::Open].iter().cloned().collect()),
//!   ..Default::default()
//! });
//!
//! match f.request(&req).unwrap() {
//!   forge::Resp::MergeRequests { mrs,.. } => {
//!     for mr in mrs {
//!       println!("{:?}", &mr);
//!     }
//!   },
//!   x => panic!("unexpected response {:?}", &x),
//! };
//! ```

mod prelude;

pub mod forge;
#[cfg(feature="gitlab")] pub mod lab;
#[cfg(feature="github")] pub mod hub;

mod util;
