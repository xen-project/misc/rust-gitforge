// Copyright 2021 Citrix
// SPDX-License-Identifier: MIT OR Apache-2.0
// There is NO WARRANTY.

// this is the *internal* prelude

pub use std::collections::hash_map::{self, HashMap};
pub use std::collections::hash_set::{self, HashSet};
pub use std::convert::{TryFrom, TryInto};
pub use std::env;
pub use std::fmt::{self, Debug};
pub use std::fs::File;
pub use std::hash::Hash;
pub use std::io::{self, BufWriter, ErrorKind, Read, Write};
pub use std::marker::PhantomData;
pub use std::mem;
pub use std::num::ParseIntError;
pub use std::path::{Path, PathBuf};
pub use std::sync::Arc;

pub use anyhow::anyhow;
pub use anyhow::Context as _;
pub use derive_more::{From, FromStr};
pub use fehler::{throw, throws};
pub use itertools::Itertools;
pub use serde::{Serialize, Deserialize, de::DeserializeOwned};
pub use strum::{Display, EnumString};
pub use thiserror::Error;

pub use crate::forge::{self, *};
pub use crate::util::*;
pub use crate::forge::Error as FE;

pub use log::{debug, info};

pub use anyhow::Error as AE;
pub fn default<T>() -> T where T: Default { Default::default() }
