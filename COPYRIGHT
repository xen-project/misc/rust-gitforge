This is "gitforge", a Rust library for accessing git "forges"
such as gitlab and github.


"gitforge" is free software: you can redistribute it and/or modify it
under the terms of the MIT licence or the Apache 2.0 Licence, at your
option.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Licences for more details.

You should have received a copy of both licences in the files
LICENSE-MIT AND LICENSE-APACHE.

For a list of the contributors to gitforge, see the git.  Information
about the contributors to the dependencies is to be found in each
dependency's source code.


Formalities
-----------

Individual files generally contain the following tag (or similar)
in the copyright notice, instead of the full licence grant text:
  SPDX-License-Identifier: MIT OR Apache-2.0
As is conventional, this should be read as a licence grant.

Contributions to gitforge are accepted based on the git commit
Signed-off-by convention, by which the contributors' certify their
contributions according to the Developer Certificate of Origin version
1.1 - see the file DEVELOPER-CERTIFICATE.

If you create a new file please be sure to add an appropriate licence
header, probably something like this:
// Copyright by contributors to the Rust "gitforge" library
// SPDX-License-Identifier: MIT OR Apache-2.0
// There is NO WARRANTY.
