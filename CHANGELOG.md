0.2.0
=====

Breaking changes:

 * `Config.token` type changed, to a new type `TokenConfig`.

 * It is no longer usually necessary to call `load_default_token`.
   To suppress the default, specify `TokenConfig::Anonymous`

 * `forge::GitLab` is no longer `From<gitlab::GitLab>`; instead,
   it's `From<(gitlab::GitLab, String)>` where the string is
   the hostname.

New:

 * New methods:
    - `host()`
    - `kind()`
    - `remote_url_to_repo()`

 * Config implements several more traits.


0.1.0
=====

Initial publish.


[comment]: # Local variables:
[comment]: # mode: text
[comment]: # End:
